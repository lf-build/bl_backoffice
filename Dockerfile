FROM node:latest

ADD /package.json /tmp/package.json
WORKDIR /tmp
RUN npm install

ADD /server /tmp/server
ADD /app /tmp/app

EXPOSE 5000

ENTRYPOINT environment=${environment} node /tmp/server/server.js