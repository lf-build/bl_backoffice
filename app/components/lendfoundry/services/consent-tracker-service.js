Polymer({
    is: "consent-tracker-service",

    properties: {
        baseUrl: {
            value: window.config.services.consentTracker,
            type: String
        }
    },

    /**
     * Saves consent given by various entities (borrowers, merchants, etc...)
     * @param {string} consentType
     * @param {string|number} entityId
     * @param {string} entityType
     * @returns {Promise<any>}
     */
    accept: function (consentType, entityId, entityType) {
        var url = "/" + entityType;
        var payload = {
            entityId: entityId,
            consent: consentType
        };
        return this.$.http.post(url, payload, this.baseUrl);
    },

    configureActivityLog: function () {
        return this.$.http.post();
    }
});
