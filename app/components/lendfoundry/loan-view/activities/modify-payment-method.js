Polymer({
    is: "modify-payment-method",

    behaviors: [
        commandBus,
        eventHub
    ],

    commands: {
        submit: function() {
            var self = this;
            var paymentMethod = self.selectedPaymentMethod;
            var referenceNumber = self.currentLoan.referenceNumber;
            var url = "/" + referenceNumber + "/Change/" + paymentMethod;

            this.$.http.post(
                url, {},
                window.config.services.loan,
                function success() {
                    var self = this;
                    self.setErrorMessage("");

                    // Show toast notification
                    self.publish("notification:show", {
                        type: "success",
                        message: "Payment method changed to " + self.selectedPaymentMethod,
                    });

                    // Pull fresh loan data
                    self.publish("loan:refresh");

                    // Close Activity
                    self.publish("activity:close", {
                        activity: self.is
                    });
                },
                function failure(response) {
                    var message = response.responseJSON.message ||
                        "Unable to process your request. Please contact customer service.";
                    self.setErrorMessage(message);
                }
            );
        },
        submitFailed: function(response) {
            throw new Error(response);
        },
        cancel: function() {
            var self = this;
            self.selectPaymentMethod(self.currentLoan.paymentMethod);
            self.publish("activity:close", {
                activity: self.is
            });
        },
        reset: function() {
            var self = this;
            self.selectPaymentMethod(self.currentLoan.paymentMethod);
        },
        initPaymentMethod: function() {
            if (!this.selectedPaymentMethod) {
                this.set("selectedPaymentMethod", this.currentLoan.paymentMethod);
            }
        },
        selectPaymentMethod: function(selectedPaymentMethod) {
            this.set("selectedPaymentMethod", selectedPaymentMethod);
        },
        setErrorMessage: function(errorMessage) {
            this.set("errorMessage", errorMessage);
        },
        resetApp: function() {
            var self = this;
            self.selectPaymentMethod(self.currentLoan.paymentMethod);
            self.publish("activity:close", {
                activity: self.is
            });
        }
    },

    properties: {
        // Definitions for radio button group
        radioButtons: {
            value: function() {
                return [{
                    label: 'ACH',
                    value: 'ACH'
                }, {
                    label: 'Check',
                    value: 'Check'
                }];
            }
        },

        alertCfg: {
            type: Object,
            value: function() {
                return {
                    type: "danger"
                };
            }
        },

        errorMessage: {
            value: ""
        },

        // Passed in from parent
        currentLoan: {
            type: Object
        }
    },

    ready: function() {
        var self = this;

        // Validate selectedPaymentMethod in concert with bankInfo
        self.subscribe("selectPaymentMethod", function() {
            var noBankErrorMessage = "No bank account information available.";
            if (self.currentLoan && self.currentLoan.bankInfo.length === 0) {
                if (self.selectedPaymentMethod.toLowerCase() === "ach") {
                    self.setErrorMessage(noBankErrorMessage);
                }
            }
        }, null);

        this.set("radioButtonClickHandler", this.selectPaymentMethod.bind(this));
        this.set("alertClickHandler", this.setErrorMessage.bind(this, ""));
    },

    detached: function() {
        this.reset();
    },

    observers: [
        "setCurrentLoan(currentLoan)"
    ],

    setCurrentLoan: function(currentLoan) {
        this.initPaymentMethod();
    },

    isCheck: function(paymentMethod) {
        return paymentMethod.toLowerCase() === "check";
    },

    shouldHideErrorMessage: function(errorMessage) {
        if (errorMessage) {
            if (errorMessage.length === 0) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    },

    shouldHideBankList: function() {
        var bankInfo = this.currentLoan.bankInfo;
        var paymentMethod = this.currentLoan.paymentMethod;
        return bankInfo.length === 0 ||
            paymentMethod.toLowerCase() !== "ach";
    },

});